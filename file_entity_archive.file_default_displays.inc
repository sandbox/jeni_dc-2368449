<?php

/**
 * Implements hook_file_default_displays().
 */
function file_entity_archive_file_default_displays() {
  $file_displays = array();
  
  // Archive default should be displayed as generic files.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'archive__default__file_field_file_default';
  $file_display->weight = 50;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['archive__default__file_field_file_default'] = $file_display;
  
  // Archive teaser should be displayed as generic files.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'archive__teaser__file_field_file_default';
  $file_display->weight = 50;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['archive__teaser__file_field_file_default'] = $file_display;
  
  // Archive previews should be displayed as generic files.
  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'archive__preview__file_field_file_default';
  $file_display->weight = 50;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $file_displays['archive__preview__file_field_file_default'] = $file_display;
  
  if (module_exists('media')) {
    // Archive previews should be displayed using a large filetype icon.
    $file_display = new stdClass();
    $file_display->api_version = 1;
    $file_display->name = 'archive__preview__file_field_media_large_icon';
    $file_display->weight = 49;
    $file_display->status = TRUE;
    $file_display->settings = '';
    $file_displays['archive__preview__file_field_media_large_icon'] = $file_display;
  }
  
  return $file_displays;
}